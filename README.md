O teste foi criado em Robot framework utilizando a IDE RIDE.
A l�gica seguida foi a seguinte:

1- Foram criadas duas Keywords, de forma que o c�digo fique visualmente mais limpo  e organizado:

START APLICATION 
open browser    ${url}    ${browser}
    Maximize Browser Window
    Wait Until Element Is Visible    ${rows}

SORT ITENS
    Drag And Drop    ${Item0}    ${Position0}
    Drag And Drop    ${Item1}    ${Position1}
    Drag And Drop    ${Item2}    ${Position2}
    Drag And Drop    ${Item3}    ${Position3}
    Drag And Drop    ${Item4}    ${Position4}
    Drag And Drop    ${Item5}    ${Position5}

2 - Dentro das Keywords sao passadas variaveis que estao definidas na Page Object Model, um ficheiro em python onde relaciona as variaveis aos objetos. (ver ficheiro variables.py)

3 - Dentro dos downloads existe um video da execu��o do teste.