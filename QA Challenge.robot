*** Settings ***
Library           SeleniumLibrary
Library           Dialogs
Variables         ../../../../variables.py

*** Test Cases ***
TC_1
    Start Aplication
    Sort Itens

*** Keywords ***
Start Aplication
    open browser    ${url}    ${browser}
    Maximize Browser Window
    Wait Until Element Is Visible    ${rows}

Sort Itens
    Drag And Drop    ${Item0}    ${Position0}
    Drag And Drop    ${Item1}    ${Position1}
    Drag And Drop    ${Item2}    ${Position2}
    Drag And Drop    ${Item3}    ${Position3}
    Drag And Drop    ${Item4}    ${Position4}
    Drag And Drop    ${Item5}    ${Position5}
